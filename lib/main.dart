import 'dart:async';
import 'dart:io';

import 'package:file_picker/file_picker.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:firebase_storage/firebase_storage.dart' as firebase_storage;
import 'package:google_sign_in/google_sign_in.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(MyApp());
}

class HomePage extends StatefulWidget {
  HomePage({Key? key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  String? _imageUrl;

  @override
  void initState() {
    super.initState();
    setState(() {
      _imageUrl = FirebaseAuth.instance.currentUser!.photoURL;
    });
  }

  Future<void> uploadFile(PlatformFile file) async {
    if (kIsWeb) {
      try {
        var uploadTask = await firebase_storage.FirebaseStorage.instance
            .ref('profile/${file.name}')
            .putData(file.bytes!);
        String imageUrl = await uploadTask.ref.getDownloadURL();
        setState(() {
          _imageUrl = imageUrl;
        });
      } on FirebaseException catch (e) {
        // e.g, e.code == 'canceled'
      }
    }else {
      try {
        var uploadTask = await firebase_storage.FirebaseStorage.instance
            .ref('profile/${file.name}')
            .putFile(File(file.path!));
        String imageUrl = await uploadTask.ref.getDownloadURL();
        setState(() {
          _imageUrl = imageUrl;
        });
      } on FirebaseException catch (e) {
        // e.g, e.code == 'canceled'
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        padding: EdgeInsets.all(32),
        children: [
          _imageUrl == null 
              ? const Icon(Icons.person) 
              : GestureDetector( 
                child:  Image.network(_imageUrl!),
                onLongPress: () async {
                  FilePickerResult? result = await FilePicker.platform.pickFiles();
                  if(result != null ) {
                    PlatformFile file = result.files.single;
                    await uploadFile(file);
                  }else {

                  }
                }
              ),
          ElevatedButton(onPressed: () {
            FirebaseAuth.instance.signOut();
          }, 
          child: Text('Sign Out')
          )
        ],
      ),
    );
  }
}

class LoginPage extends StatefulWidget {
  LoginPage({Key? key}) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  Future<UserCredential> signInWithGoogle() async {
    // Create a new provider
    GoogleAuthProvider googleProvider = GoogleAuthProvider();

    googleProvider
        .addScope('https://www.googleapis.com/auth/contacts.readonly');
    googleProvider.setCustomParameters({'login_hint': 'user@example.com'});

    // Once signed in, return the UserCredential
    return await FirebaseAuth.instance.signInWithPopup(googleProvider);

    // Or use signInWithRedirect
    // return await FirebaseAuth.instance.signInWithRedirect(googleProvider);
  }
  Future<UserCredential> signInWithGoogleNative() async {
  // Trigger the authentication flow
  final GoogleSignInAccount? googleUser = await GoogleSignIn().signIn();

  // Obtain the auth details from the request
  final GoogleSignInAuthentication googleAuth = await googleUser!.authentication;

  // Create a new credential
  final credential = GoogleAuthProvider.credential(
    accessToken: googleAuth.accessToken,
    idToken: googleAuth.idToken,
  );

  // Once signed in, return the UserCredential
  return await FirebaseAuth.instance.signInWithCredential(credential);
}
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        padding: EdgeInsets.all(32),
        children: [
          ElevatedButton(onPressed: () async {
            if(kIsWeb) {
              await signInWithGoogle();
            }else {
              signInWithGoogleNative();
            }
          }, 
          child: Text('Sign In'))
        ],
      ),
    );
  }
}

class UnknownPage extends StatefulWidget {
  UnknownPage({Key? key}) : super(key: key);

  @override
  _UnknownPageState createState() => _UnknownPageState();
}

class _UnknownPageState extends State<UnknownPage> {
  @override
  Widget build(BuildContext context) {
    return Container(color: Colors.red);
  }
}

class MyApp extends StatefulWidget {
  MyApp({Key? key}) : super(key: key);

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  final _navigatorKey = GlobalKey<NavigatorState>();
  late StreamSubscription<User?> _sub;
  @override
  void initState() {
    super.initState();
    _sub = FirebaseAuth.instance.authStateChanges().listen((user) {
      _navigatorKey.currentState!.pushReplacementNamed(
        user != null ? 'home' : 'login'
      );
    });
  }

  @override
  void dispose() {
    _sub.cancel();
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'HelloWord',
      navigatorKey: _navigatorKey,
      initialRoute: 
      FirebaseAuth.instance.currentUser == null ? 'login' : 'home',
      onGenerateRoute: (settings) {
        switch(settings.name) {
          case 'home':
            return MaterialPageRoute(
                settings: settings, builder: (_) => HomePage()
            );
          case 'login':
            return MaterialPageRoute(
                settings: settings, builder: (_) => LoginPage()
            );
          default:
            return MaterialPageRoute(
                  settings: settings, builder: (_) => UnknownPage()
            );
        }
      },
    );
  }
}
